﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum Direction
{
    STILL = 0,
    UP = 1,
    DOWN = -1
}
