﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

	[SerializeField]
	float forceValue = 4.5f;
    float minBallSpeed = 4.5f;
    float ballSpeedIncrease = 0.1f;
    Rigidbody2D myBody;
	// Use this for initialization
	void Start ()
    {
        myBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnCollisionExit2D(Collision2D other)
    {
        // Make sure x axis speed doesn't go below 4.5f
        float xSpeed = myBody.velocity.x;
        if (xSpeed < 0)
        {
            xSpeed = (xSpeed > -minBallSpeed) ? -minBallSpeed : xSpeed;
            xSpeed -= ballSpeedIncrease;
        }
        else if (xSpeed > 0)
        {
            xSpeed = (xSpeed < minBallSpeed) ? minBallSpeed : xSpeed;
            xSpeed += ballSpeedIncrease;
        }
        myBody.velocity = new Vector2(xSpeed, myBody.velocity.y);
    }

    public void Reset()
    {
        // reset the ball position and restart the ball movement
        myBody.velocity = Vector2.zero;
		transform.position = new Vector2(0,0);
	}

	public void Stop()
    {
        // this method stops the ball
        myBody.velocity = Vector2.zero;
	}

    public void StartBall()
    {
        myBody.AddForce(new Vector2(forceValue * 50, 50));
        forceValue = forceValue * -1;
    }
}
