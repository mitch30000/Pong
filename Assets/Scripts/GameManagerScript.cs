﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

    [SerializeField]
    BallScript gameBall;
    [SerializeField]
    Text scoreText;
    [SerializeField]
    AudioClip goalScored;
    [SerializeField]
    AudioClip endGame;
    [SerializeField]
    GameObject endGameScreen;
    [SerializeField]
    GameObject waitingToStartScreen;

    AudioSource audioSource;

    int playerOneScore, playerTwoScore;

    bool waitingToStart;

	// Use this for initialization
	void Start () {
        Debug.Log("GameManagerScript Start");
        audioSource = GetComponent<AudioSource>();
        StartNewGame();
    }
	
	void FixedUpdate () {
	    if(waitingToStart && Input.GetKey(KeyCode.Space))
        {
            waitingToStart = false;
            gameBall.StartBall();
            waitingToStartScreen.SetActive(false);
        }
	}

    void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    void UpdateScoreText()
    {
        scoreText.text = "Player One " + playerOneScore.ToString() + " - " + playerTwoScore.ToString() + " Player Two";
    }

    public void GoalScored(int playerNumber)
    {
        PlaySound(goalScored);
        if(playerNumber == 1)
        {
            playerOneScore++;
        }
        else if (playerNumber == 2)
        {
            playerTwoScore++;
        }

        if(playerOneScore == 3)
        {
            GameOver(1);
        }
        else if (playerTwoScore == 3)
        {
            GameOver(2);
        }
        else
        {
            gameBall.Reset();
            waitingToStart = true;
            waitingToStartScreen.SetActive(true);
        }
        UpdateScoreText();
    }

    public void StartNewGame()
    {
        playerOneScore = 0;
        playerTwoScore = 0;
        UpdateScoreText();
        endGameScreen.SetActive(false);
        gameBall.Reset();
        waitingToStart = true;
        waitingToStartScreen.SetActive(true);
    }

    void GameOver(int winner)
    {
        gameBall.Stop();
        endGameScreen.SetActive(true);
        PlaySound(endGame);
    }
}
