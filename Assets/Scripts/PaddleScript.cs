﻿using UnityEngine;
using System.Collections;

public class PaddleScript : MonoBehaviour
{

    [SerializeField]
    bool isPlayerTwo;
    [SerializeField]
    float paddleSpeed = 0.2f;       // how far the paddle moves per frame
    Transform myTransform;    // reference to the object's transform
    Direction direction = Direction.STILL;
    float previousPositionY;
    GameObject topWall, bottomWall;

    // Use this for initialization
    void Start()
    {
        myTransform = transform;
        previousPositionY = myTransform.position.y;
        topWall = GameObject.Find("WallTop");
        bottomWall = GameObject.Find("WallBottom");
    }

    // FixedUpdate is called once per physics tick/frame
    void FixedUpdate()
    {
        // first decide if this is player 1 or player 2 so we know what keys to listen for
        if (isPlayerTwo)
        {
            if (Input.GetKey(KeyCode.O))
                MoveUp();
            else if (Input.GetKey(KeyCode.L))
                MoveDown();
        }
        else // if not player 2 it must be player 1
        {
            if (Input.GetKey(KeyCode.Q))
                MoveUp();
            else if (Input.GetKey(KeyCode.A))
                MoveDown();
        }
        if (previousPositionY > myTransform.position.y)
            direction = Direction.DOWN;
        else if (previousPositionY < myTransform.position.y)
            direction = Direction.UP;
        else
            direction = Direction.STILL;
    }

    void LateUpdate()
    {
        previousPositionY = myTransform.position.y;
    }

    void OnCollisionExit2D(Collision2D other)
    {
        float adjust = 5 * (int)direction;
        other.rigidbody.velocity = new Vector2(other.rigidbody.velocity.x, other.rigidbody.velocity.y + adjust);
    }

    // move the player's paddle up by an amount determined by 'speed'
    void MoveUp()
    {
        if ((myTransform.position.y + myTransform.localScale.y) <= (topWall.transform.position.y - topWall.transform.localScale.y)) {
            myTransform.position = new Vector2(myTransform.position.x, myTransform.position.y + paddleSpeed);
        }
    }

    // move the player's paddle down by an amount determined by 'speed'
    void MoveDown()
    {
        if ((myTransform.position.y - myTransform.localScale.y) >= (bottomWall.transform.position.y + bottomWall.transform.localScale.y))
        {
            myTransform.position = new Vector2(myTransform.position.x, myTransform.position.y - paddleSpeed);
        }
    }
}