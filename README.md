Classic Pong game built using Unity.  

### Building  
In order to build/run this project you will need to download Unity version 5.2.1 
and run the project using that version (more information on downloading, building 
and running using Unity can be found [here](https://unity.com/)).

### Playing
This version of Pong supports two players, a Red player and a Blue player.  The
Red player uses 'Q' and 'A' to move their paddle while the Blue player uses 'O'
and 'L' to move their paddle.  First player to 3 points wins!